package com.tuxkiddos.apps.dialogfragmentproject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

public class MyDailogFragment extends DialogFragment implements OnClickListener {

	Button btnOk, btnCancel;

	CheckBox checkBox1;

	DialogCommunicator communicator;

	public interface DialogCommunicator {
		public void onDialogMessage(String string);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		communicator = (DialogCommunicator) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().setTitle("My Dialog Title");
		View view = inflater.inflate(R.layout.fragment_mydialog, null);
		btnOk = (Button) view.findViewById(R.id.btnOK);
		btnCancel = (Button) view.findViewById(R.id.btnCancel);
		checkBox1 = (CheckBox) view.findViewById(R.id.checkBox1);
		btnOk.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		setCancelable(false);
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCancel:
			dismiss();
			communicator.onDialogMessage("You cancelled the operation.");
			break;
		case R.id.btnOK:
			dismiss();
			String erase_option = "";
			if (checkBox1.isChecked()) {
				erase_option = "permanently";
			}
			else {
				erase_option = "temporarily";
			}
			StringBuilder response = new StringBuilder("Data Erased ");
			response.append(erase_option);
			response.append(".");
			communicator.onDialogMessage(response.toString());
			break;
		default:
			break;
		}

	}

}
