package com.tuxkiddos.apps.dialogfragmentproject;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tuxkiddos.apps.dialogfragmentproject.MyDailogFragment.DialogCommunicator;

public class MainActivity extends FragmentActivity implements DialogCommunicator{
	Button btnShowDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btnShowDialog=(Button)findViewById(R.id.btnShowDialog);
		btnShowDialog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FragmentManager manager=getSupportFragmentManager();
				MyDailogFragment myDialog=new MyDailogFragment();
				myDialog.show(manager, "myDialog");
				
			}
		});
	}
	@Override
	public void onDialogMessage(String string) {
		Toast.makeText(MainActivity.this, string, Toast.LENGTH_SHORT).show();
		
	}
	
}
